

//tree Template in java

package BinaryTree;

import java.util.Scanner;

public class BTree{
	public class Node{
		int data;
		Node left=null;
		Node right=null;

		Node(int data){
			this.data=data;
		}
	}

	
	public Node addNode(int level){
		Scanner sc = new Scanner(System.in);

		Node newNode = null;

		
		
		if(level==0)
			System.out.println("Enter data at root");
		else
			System.out.println("Enter data at level " + level);
		
		int data = sc.nextInt();
		
		newNode = new Node(data);
		

		
		if(level==0)
			System.out.println("Do you want to add left node at root ");
		else
			System.out.println("Do you want to add left node at level " + level);
		
		char ch = sc.next().charAt(0);

		
		if(ch=='y' || ch=='Y'){
			newNode.left = addNode(++level);
		}
		
		
		
		if(level==0)
			System.out.println("Do you want to add right node at root ");
		else
			System.out.println("Do you want to add right node at level " + level);
		
		ch = sc.next().charAt(0);

		if(ch=='y' || ch=='Y'){
			newNode.right = addNode(++level);
		}
		
		
		return newNode;
	}

	static boolean searchElement(int no,Node root){
		if(root==null){
			System.out.println("IN IF");
			return false;
		}

		if(root.data==no){
			return true;
		}else{
			BTree.searchElement(no,root.left);
			return BTree.searchElement(no,root.right);
		}
	}

	
	public static void preOrder(Node root){
		if(root==null){
			return;
		}

		System.out.println(root.data);
		BTree.preOrder(root.left);
		BTree.preOrder(root.right);
	}

	public static void inOrder(Node root){
		if(root==null){
			return;
		}
		
		BTree.inOrder(root.left);
		System.out.println(root.data);
		BTree.inOrder(root.right);
	}
	
	public static void postOrder(Node root){
		if(root==null){
			return;
		}
		
		BTree.postOrder(root.left);
		BTree.postOrder(root.right);
		System.out.println(root.data);
	}
	
	class Queue{
		Node data;
		Queue next;
		
		Queue(Node data){
			this.data=data;
		}
	}

	static Queue front = null;
       	static Queue rear = null;	
	
	static boolean isEmpty(){
		if(BTree.front==null && BTree.rear== null){
			return true;
		}else
			return false;
	}

	static void push(Node root){
		BTree obj = new BTree();
		Queue node = obj.new Queue(root);
		node.next=null;

		if(BTree.front==null && BTree.rear==null){
			BTree.front = node;
			BTree.rear = node;
		}else{
			BTree.rear.next=node;
			BTree.rear=node;
		}
	}

	static Node pop(){
		Node obj = BTree.front.data;

		if(BTree.front==BTree.rear){
			BTree.front=null;
			BTree.rear=null;
		}else{
			BTree.front=BTree.front.next;
		}

		return obj;
	}
	
	public static void levelOrder(Node root){
		if(root==null){
			return;
		}
		
		BTree.push(root);

		while(!isEmpty()){
			Node retVal = BTree.pop();
			System.out.print("	" + retVal.data);
			
			if(retVal.left!=null){
				BTree.push(retVal.left);
			}

			if(retVal.right!=null){
				BTree.push(retVal.right);
			}
		}
	}

	public static void cases(){
	        Scanner sc = new Scanner(System.in);

                BTree BTreeObj = new BTree();

                BTree.Node root = null;

                char ch;

                do{
                        System.out.println("1 : Add new TreeNode");
                        System.out.println("2 : Search Tree Node");
                        System.out.println("3 : print PreOrder Tree");
                        System.out.println("4 : print InOrder Tree");
                        System.out.println("4 : print PostOrder Tree");
                        System.out.println("4 : print LevelOrder Tree");

                        System.out.println("Enter your Choice");
                        int choice = sc.nextInt();

                        switch(choice){
                                case 1 : root = BTreeObj.addNode(0);
                                break;

                                case 2 :{
					if(root==null)
						System.out.println("SORRY!!!!,Tree is Empty");
					else{
                                      	 	 System.out.println("Enter No for Search");
                                        	 int no = sc.nextInt();

                                        	 boolean ret = BTree.searchElement(no,root);

                                       		 if(ret)
                                                	System.out.println("Given Element is present in this Tree");
                                        	 else
                                                	System.out.println("oops!!!, given no is not in this Tree");
					}
					
					System.out.println();
					System.out.println("     *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*");
					System.out.println();
					System.out.println();
                                }
                                break;

                                case 3 : {
						if(root==null)
							System.out.println("SORRY!!!!,Tree is Empty");
						else{
							System.out.print("[");
							BTree.preOrder(root);
							System.out.print("]");
						}
						System.out.println();
						System.out.println("     *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*");
						System.out.println();
						System.out.println();
					 }
				break;

                                case 4 :{
						if(root==null)
							System.out.println("SORRY!!!!,Tree is Empty");
						else{
							System.out.print("[");
					       		BTree.inOrder(root);
							System.out.print("]");
							}
						System.out.println();
						System.out.println("     *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*");
						System.out.println();
						System.out.println();
					}
				break;

                                case 5 : {
						if(root==null)
							System.out.println("SORRY!!!!,Tree is Empty");
						else{
							System.out.print("[");
							BTree.postOrder(root);
							System.out.print("]");
							}
						System.out.println();
						System.out.println("     *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*");
						System.out.println();
						System.out.println();
					 }
				break;

                                case 6 : {
						if(root==null)
							System.out.println("SORRY!!!!,Tree is Empty");
						else{
							System.out.print("[");
						 	BTree.levelOrder(root);
							System.out.print("]");
						}
						System.out.println();
						System.out.println();
						System.out.println("     *-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*");
						System.out.println();
						System.out.println();
					 }
				break;

                        }
			
			System.out.println();
			System.out.println();
                        System.out.println("Do you Want to Continue");
                        ch = sc.next().charAt(0);
                }while(ch=='y'|| ch=='Y');
		
	}
}
