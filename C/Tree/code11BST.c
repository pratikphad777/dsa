

#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

struct BSTNode{
	int data;
	struct BSTNode* left;
	struct BSTNode* right;
};

void printBST(struct BSTNode* root){
	if(root!=NULL){
		printBST(root->left);
		printf("%d\n",root->data);
		printBST(root->right);
	}
}
bool searchBST(struct BSTNode* root,int ele){
	if(root == NULL){
		return false;
	}

	if(root->data == ele){
		return true;
	}

	if(root->data>ele){
		searchBST(root->left,ele);
	}else{
		searchBST(root->right,ele);
	}
}

struct BSTNode* insertBST(struct BSTNode* root,int data){
	if(root == NULL){
		struct BSTNode* newNode = malloc(sizeof(struct BSTNode));
		newNode->data = data;
		newNode->left = NULL;
		newNode->right = NULL;
		return newNode;
	}

	if(root->data>data){
		root->left = insertBST(root->left,data);
	}else{
		root->right = insertBST(root->right,data);
	}

	return root;
}

int maxBST(struct BSTNode* root){
	if(root->right==NULL){
		return root->data;
	}

	maxBST(root->right);
}

int minBST(struct BSTNode* root){
	if(root->left==NULL){
		return root->data;
	}

	minBST(root->left);
}

struct BSTNode* maxnode(struct BSTNode* root){
	if(root->right==NULL){
		return root;
	}
	struct BSTNode* y = maxnode(root->right);

	if(y==root->right){
		root->right=NULL;
	}
}


struct BSTNode* deleteNode(struct BSTNode* root,int ele){
	if(root == NULL){
		return NULL;
	}

	if(root->data == ele){
		if(root->left==NULL && root->right!=NULL){
			struct BSTNode* temp = root;
			root=root->right;
			free(temp);
		}else if(root->right==NULL && root->left!=NULL){
			
			struct BSTNode* temp = root;
			root=root->left;
			free(temp);
		}else if(root->right == NULL && root->left==NULL){
			free(root);
			root=NULL;
		}else{
			struct BSTNode* max = maxnode(root->left);
			max->left=root->left;
			max->right=root->right;
			free(root);
			root=max;
		}
		return root;
	}else if(root->data>ele){
		root->left=deleteNode(root->left,ele);
	}else{
		root->right=deleteNode(root->right,ele);
	}

	return root;
}

bool validateBST(struct BSTNode* root){
	if(root==NULL)
		return true;
	if(root->left!=NULL){
		if(root->data>root->left->data){
			validateBST(root->left);
		}else{
			return false;
		}
	}
	
	if(root->right!=NULL){
		if(root->data<root->right->data){
			validateBST(root->left);
		}else{
			return false;
		}
	}

	return true;


}
int data;
int predessor(struct BSTNode* root,int k){
	if(root==NULL){
		return 0;
	}
	
	if(root->data==k){
		if(root->right!=NULL)
			return root->right->data;
		return data;
	}

	if(root->data<k)
		predessor(root->right,k);
	else{
		data=root->data;
		predessor(root->left,k);
	}
}

int successor(struct BSTNode* root,int k){
	if(root==NULL){
		return 0;
	}
	
	if(root->data==k){
		if(root->left!=NULL)
			return root->left->data;
		return data;
	}

	if(root->data<k){
		data=root->data;
		successor(root->right,k);
	}else
		successor(root->left,k);
}

void main(){
	struct BSTNode* root = NULL;
	
	char ch;
	do{
		printf("0 : printBST\n");
		printf("1 : searchBST\n");
		printf("2 : insertBST\n");
		printf("3 : maxBST\n");
		printf("4 : mintBST\n");
		printf("5 : deleteBST\n");
		printf("6 : validateBST\n");
		printf("7 : predessor\n");
		printf("8 : successor\n");
		printf("9 : k smallest\n");
		printf("10 : k gretest\n");

		int choice;
		printf("Enter choice\n");
		scanf("%d",&choice);

		switch(choice){
			case 0 : printBST(root);
				 break;

			case 1 : {
				       printf("Enter search element\n");
				       int ele;
				       scanf("%d",&ele);
				       bool sel = searchBST(root,ele);

				       if(sel){
				       		printf("found\n");
				       }else{
				       		printf("Not found\n");
				       }
			       }
			       break;
			
			case 2 : {
					 printf("Enter data\n");
					 int data;
					 scanf("%d",&data);
					 root = insertBST(root,data);
				 }
				 break;

			case 3 :{
					int max = maxBST(root);
					printf("max ele is %d\n",max);
				}
				break;
			case 4 :{
					int min = minBST(root);
					printf("min ele is %d\n",min);
				}
				break;
			case 5 :{
					printf("Enter element\n");
					int ele;
					scanf("%d",&ele);
					root=deleteNode(root,ele);
				}
				break;
			case 6 :{
					bool ret = validateBST(root);
					if(ret)
						printf("BST\n");
					else
						printf("Not BST");
				}
				break;
			case 7 :{
					int n;
					printf("Enter number\n");
					scanf("%d",&n);
					int num = predessor(root,n);
					printf("predessor of %d is %d\n",n,num);
				}
				break;
			
			case 8 :{
					int n;
					printf("Enter number\n");
					scanf("%d",&n);
					int num = successor(root,n);
					printf("successor of %d is %d\n",n,num);
				}
				break;
		}

		printf("Continue?\n");
		getchar();
		scanf("%c",&ch);
	}while(ch=='y' || ch=='Y');
}
