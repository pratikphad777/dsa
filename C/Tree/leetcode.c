//given the root of a binary tree, return an array of the largest value in each row of the tree (0-indexed).

#include<stdio.h>
#include<stdlib.h>

struct node{
	int data;
	struct node* left;
	struct node* right;
};

void print(struct node* root){
	if(root == NULL){
		return;
	}

	printf("%d\n",root->data);
	print(root->left);
	print(root->right);
}

struct node* addNode(struct node* root){
	printf("Enter data\n");
	
	struct node* newNode = malloc(sizeof(struct node*));
	scanf("%d",&newNode->data);

	printf("Do yo want to add left\n");
	char c;
	getchar();
	scanf("%c",&c);

	if(c=='y'){
		newNode->left = addNode(newNode);
	}else{
		newNode->left=NULL;
	}
	
	printf("Do yo want to add right\n");
	getchar();
	scanf("%c",&c);

	if(c=='y'){
		newNode->right = addNode(newNode);
	}else{
		newNode->right=NULL;
	}

	return newNode;
}

int i=0;
int* count(struct node* root,int* num){
	if(root==NULL){
		return num;
	}
	i++;
	
	printf("i = %d\n",i);
	num=realloc(num,i*sizeof(int));
	printf("num = %ld\n",sizeof(*num));
	num[i]=root->data;

	count(root->left,num);
	count(root->right,num);
	i--;

	if(root->data>num[i]){
		num[i]=root->data;
	}
	printf("i = %d\n",i);

	return num;


}

void main(){
	struct node* root = malloc(sizeof(struct node*));

	root->data=1;

	root->left=addNode(root);
	root->right=addNode(root);
	
	int* num = calloc(0,sizeof(int));
	
	int* arr = count(root,num);

	print(root);

	for(int i=0;i<sizeof(*arr)/4;i++){
		printf("%d  ",*(arr)+i);
	}
	printf("\nsize of arr %ld\n",sizeof(*arr));

}
