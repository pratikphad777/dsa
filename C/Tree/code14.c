
#include<stdlib.h>
#include<stdio.h>

struct TreeNode{
	int data;
	struct TreeNode *left;
	struct TreeNode *right;
};

struct TreeNode* createNode(int level){
	level++;
	struct TreeNode* newNode = malloc(sizeof(struct TreeNode));

	printf("Enter data at level %d\n",level);
	scanf("%d",&newNode->data);

	printf("Do you want to add node at %d level left\n",level);
	char ch;
	getchar();
	scanf("%c",&ch);
	
	if(ch=='Y'||ch=='y'){
		newNode->left = createNode(level);
	}
	
	printf("Do you want to add node at %d level right\n",level);
	getchar();
	scanf("%c",&ch);
	
	if(ch=='Y'||ch=='y'){
		newNode->right = createNode(level);
	}

	return newNode;
	
}

void printNode(struct TreeNode* root){
	if(root == NULL){
		return;
	}

	printf("%d\n",root->data);
	printNode(root->left);
	printNode(root->right);
}

void main(){

	struct TreeNode* root = malloc(sizeof(struct TreeNode));
	printf("Enter data at root node\n");
	scanf("%d",&root->data);

	printf("Do you want to add node at root left\n");
	char ch;
	getchar();
	scanf("%c",&ch);
	
	if(ch=='Y'||ch=='y'){
		root->left = createNode(0);
	}
	
	printf("Do you want to add node at root right\n");
	getchar();
	scanf("%c",&ch);
	
	if(ch=='Y'||ch=='y'){
		root->right = createNode(0);
	}


	printNode(root);

}
