
class GFG{
	static int[] smallest(int nums[]){
		int arr[]=new int[]{-1,-1};

		for(int i=0;i<nums.length;i++){
			if(nums[i]<arr[0]){
				arr[1]=arr[0];
				arr[0]=nums[i];
			}

			if(nums[i]>arr[0]&&nums[i]<arr[1]){
				arr[1]=nums[i];
			}
		}
		return arr;
	}
}

class Main{
	
	public static void main(String[] args){
		int nums[] = new int[]{5,9,4,2,8,6,4,1,7};

		int arr[] = GFG.smallest(nums);

		System.out.println("smallest " + arr[0] + "Second smallest" + arr[1]);
	}
}
