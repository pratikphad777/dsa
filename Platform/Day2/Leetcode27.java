
class Leetcode{
	static int remove(int nums[],int val){
		int k=0;
		int j=nums.length-1;
		for(int i=0;i<nums.length;i++){
			
			while(nums[j]==val){
				j--;
			}

			if(nums[i]==val){
				nums[i]=nums[j];
				k++;
				j--;
			}
		}
		return nums.length-k;
	}
}

class Main{
	public static void main(String []args){
		int nums[] = new int[]{0,1,2,2,3,0,4,2};
		int val = 2;

		int k = Leetcode.remove(nums,val);

		for(int i=0;i<nums.length-k;i++){
			System.out.println(nums[i]);
		}
	}
}
