class Leetcode{
	static int[] sum(int nums[],int val){

		int arr[] = new int[]{-1,-1};
		for(int i=0;i<nums.length;i++){
			for(int j=i+1;j<nums.length;j++){
				if(nums[i]+nums[j]==val){
					arr[0]=i;
					arr[1]=j;
					return arr;
				}
			}
		}
		return arr;
	}
}

class Main{
	public static void main(String args[]){
		int arr[] = new int[]{1,2,3,4,5,6};
		int num = 10;

		int nums[] = Leetcode.sum(arr,num);

		System.out.println("indexes are "+ nums[0] +" "+ nums[1]);
	}
}
