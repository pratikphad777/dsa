//mejority element

class GFG{
	static int mejority(int nums[]){
		int ptr=0;
		int val=0;

		for(int i=0;i<nums.length;i++){
			int temp=0;
			for(int j=i;j<nums.length;j++){
				if(nums[i]==nums[j]){
					temp++;
				}
			}
			if(temp>ptr){
				ptr=temp;
				val=nums[i];
			}
			if(temp==ptr){
				val=-1;
			}
		}
		return val;
	}
}

class Main{
	public static void main(String[] args){
		int arr[]=new int[]{1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,3,3,3,3,1,1,1,1};

		int val = GFG.mejority(arr);

		System.out.println("mejority element is " + val);
	}
}
