import java.io.*;

class LeetCode{
	static int[] merge(int nums1[],int nums2[]){
		
		int k=0;
		int j=0;
		int l=0;
		//     [1,2,3,0,0,0] [4,5,6]
		for(int i=0;i<nums1.length;i++){
			
			if(j<nums2.length && nums2[j]<nums1[k] && nums2[j]<=nums2[l]){
				int temp=nums1[k];
				nums1[k]=nums2[j];
				nums2[j]=temp;
				j++;
					
			}else if(j<nums2.length && nums2[l]<nums1[k] && nums2[l]<nums2[j]){
					int temp = nums1[k];
					nums1[k]=nums2[l];
					nums2[l]=temp;
					l++;
					if(l>=j){
						l=0;
					}
			}else if(l<nums2.length && j>=nums2.length){
				int temp = nums1[k];
				nums1[k]=nums2[l];
				nums2[l]=temp;
				l++;
				if(l>=j)
					l=0;
			}else if(k>0 && nums[k-1]>0 && nums1[k]==0){
				//[1,2,3,0,0,0]  [4,5,6]
				if(j<nums2.length && nums2[l]<nums2[j]){
					nums1[k]=nums2[l];
					nums2[l]=Integer.MAX_VALUE;
					l++;
					if(l>=0)
						l=0;
				}else{ 
					nums1[k]=nums2[j];
					j++;
					if(j==l)
						l++;
				}
			}
			k++;
		}
		return nums1;
	}
}

class Main{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter num1 Size");
		int s1 = Integer.parseInt(br.readLine());

		System.out.println("Enter num2 Size");
		int s2 = Integer.parseInt(br.readLine());

		int nums1[] = new int[s1];
		int nums2[] = new int[s2];

		System.out.println("array1");
		for(int i=0;i<s1;i++)
			nums1[i] = Integer.parseInt(br.readLine());
		
		System.out.println("array2");
		for(int i=0;i<s2;i++)
			nums2[i] = Integer.parseInt(br.readLine());

		nums1=LeetCode.merge(nums1,nums2);
			

		for(int i=0;i<nums1.length;i++){
			System.out.print(nums1[i]+" ");
		}

		System.out.println();
	}
}
