class Solution {
    public int searchInsert(int[] nums, int target) {
        int start=0;
        int end=nums.length-1;
        int mid=0;
        while(start<=end){
            mid=(start+end)/2;
            if(nums[mid]==target){
                return mid;
            }
            if(nums[mid]<target){
                start=mid+1;
            }else{
                end=mid-1;
            }
        }

        if(nums[mid]<target)
            mid++;
        else if(nums[0]>target)
            mid=0;
        return mid;   
    }
}
