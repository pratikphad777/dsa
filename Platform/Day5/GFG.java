
import java.io.*;

class GFGCode{
	static int pivot(int[] nums){
		int pivot=0;

		for(int i=0;i<nums.length;i++){
			int temp=0;
			for(int j=i+1;j<nums.length;j++){
				temp=temp+nums[j];
			}
			if(temp==pivot)
				return i;
			pivot=pivot+nums[i];
		}
		
			return -1;
	}
}

class GFG{
	static int pivot(int nums[]){
		for(int i=1;i<nums.length;i++){
			nums[i]=nums[i]+nums[i-1];
		}
		
		if(nums[nums.length-1]-nums[0]==0){
			return 0;
		}
		for(int i=1;i<nums.length;i++){
			if(nums[i-1]==nums[nums.length-1]-nums[i]){
				return i;
			}
		}
		return -1;
	}
}

class Main{
	public static void main(String []args)throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int arr[] = new int[3];
		
		System.out.println("Enter arr element");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		int index = GFG.pivot(arr);	
		System.out.println("Pivot index is " + index);
	}
}
