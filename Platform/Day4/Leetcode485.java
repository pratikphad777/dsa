class Leetcode{
	static int cons(int nums[]){
		int ptr=0;
		int temp=0;
		for(int i=0;i<nums.length;i++){
			if(nums[i]==1)
				temp++;
			else{
				if(temp>ptr)
					ptr=temp;
				temp=0;
			}
		}
		if(temp>ptr)
			ptr=temp;
		return ptr;
	}
}

class Main{
	public static void main(String []args){
		int arr[] = new int[]{1,1,1,0,1,1,0,1,1,1,1};

		int num = Leetcode.cons(arr);

		System.out.println("1's are "+num);
	}
}
