
import java.io.*;

class LeetCode{
	static int GC(int nums[]){
		int great = Integer.MIN_VALUE;
		int sec = Integer.MIN_VALUE;
		int third = Integer.MIN_VALUE;
		int l1 = 0;
		int l2 = 0;

		for(int i=0;i<nums.length;i++){
			if(nums[i]<l1){
				l2=l1;
				l1=nums[i];
			}else if(nums[i]<l2){
				l2=nums[i];
			}

			if(nums[i]>great){
				third=sec;
				sec=great;
				great=nums[i];
			}else if(nums[i]>sec){
				third=sec;
				sec=nums[i];
			}else if(nums[i]>third){
				third=nums[i];
			}
		}
		if(great>0 && l1*l2>sec*third)
			return l1*l2*great;
		return great*sec*third;
	}
}
class Main{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter array Size");
		int s = Integer.parseInt(br.readLine());
		
		int arr[]=new int[s];

		for(int i=0;i<s;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		int num = LeetCode.GC(arr);

		System.out.println("The great candidate is " + num);
	}
}
