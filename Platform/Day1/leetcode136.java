import java.util.HashMap;
import java.io.*;

class LeetCode{
	static int nums(int num[]){
		HashMap<Integer,Integer> hm = new HashMap<Integer,Integer>();

		for(int i=0;i<num.length;i++){
			if(hm.containsKey(num[i])){
				hm.put(num[i],2);
			}else{
				hm.put(num[i],1);
			}
		}

		for(int i=0;i<num.length;i++){
			Object value = hm.get(num[i]);
			int val = (int)value;
			if(val==1)
				return num[i];
		}

		return 0;
	}
}

class Main{
	public static void main(String args[])throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int arr[]=new int[7];
		
		System.out.println("Enter array value");
		for(int i=0;i<7;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		int num = LeetCode.nums(arr);
		System.out.println("single element is " + num);
	}
}
