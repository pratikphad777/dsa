import java.io.*;

class GFG{
	static boolean key(int nums[],int val){

		for(int i=0;i<nums.length;i++){
			for(int j=i+1;j<nums.length;j++){
				if(nums[i]+nums[j]==val)
					return true;
			}
		}
		
		return false;
	}
}

class Main{
        public static void main(String args[])throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                int arr[]=new int[7];

                System.out.println("Enter array value");
                for(int i=0;i<7;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }
		System.out.println("Enter value");
		int value = Integer.parseInt(br.readLine());

                boolean num = GFG.key(arr,value);
                
			if(num)
				System.out.println("elements are present");
			else
				System.out.println("elements not present");

        }
}

