//1st Reapting

import java.io.*;
import java.util.ArrayList;

class GFG{
	static int nums(int num[]){
		ArrayList<Integer> l = new ArrayList<Integer>();
		
		for(int i=0;i<num.length;i++){
			if(l.contains(num[i])){
				return num[i];
			}else{
				l.add(num[i]);
			}
		}
		return 0;
	}
}

class Main{
        public static void main(String args[])throws IOException{
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                int arr[]=new int[7];

                System.out.println("Enter array value");
                for(int i=0;i<7;i++){
                        arr[i]=Integer.parseInt(br.readLine());
                }

                int num = GFG.nums(arr);
                System.out.println("first repiting element is " + num);
        }
}

